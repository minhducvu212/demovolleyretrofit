package com.example.minhd.demoretrofitvolley.Comon;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;

import com.example.minhd.demoretrofitvolley.R;
import com.example.minhd.demoretrofitvolley.Retrofit.RetrofitActivity;
import com.example.minhd.demoretrofitvolley.Volley.VolleyActivity;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText edtID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        findViewByIds();
    }

    private void findViewByIds() {
        findViewById(R.id.btn_retro).setOnClickListener(this);
        findViewById(R.id.btn_volley).setOnClickListener(this);
        findViewById(R.id.btn_request).setOnClickListener(this);
        edtID = (EditText) findViewById(R.id.edt_ID) ;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_retro:
                Intent intent = new Intent(MainActivity.this, RetrofitActivity.class);
                startActivity(intent);
                break;

            case R.id.btn_volley:
                Intent intent1 = new Intent(MainActivity.this, VolleyActivity.class);
                startActivity(intent1);
                break;

            case R.id.btn_request:
                Intent intent2 = new Intent(MainActivity.this, RequestActivity.class);
                Bundle bundle = new Bundle();
                bundle.putInt("ID", Integer.parseInt(edtID.getText().toString()) );
                intent2.putExtra("id", bundle);
                startActivity(intent2);
                break;
            default:

        }
    }


}
