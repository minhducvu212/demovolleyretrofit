package com.example.minhd.demoretrofitvolley.Comon;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.minhd.demoretrofitvolley.R;
import com.example.minhd.demoretrofitvolley.Retrofit.ApiConnect;
import com.example.minhd.demoretrofitvolley.Retrofit.ApiUtils;
import com.example.minhd.demoretrofitvolley.Volley.VolleySingleton;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Post extends AppCompatActivity implements View.OnClickListener {

    private EditText edtFirstName, edtLastName, edtAvatar;
    private String TAG = "POST";
    private ApiConnect apiConnect;
    private AlertDialog.Builder alertDialogBuilder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post);
        findViewByIds();
        setEvents();
        alertDialogBuilder = new AlertDialog.Builder(this);

    }

    private void findViewByIds() {
        edtFirstName = (EditText) findViewById(R.id.edt_firstName);
        edtLastName = (EditText) findViewById(R.id.edt_firstName);
        edtAvatar = (EditText) findViewById(R.id.edt_ava);
        findViewById(R.id.btn_submit).setOnClickListener(this);

    }

    private void setEvents() {

    }

    @Override
    public void onClick(View view) {
        showDiaLog();
    }

    private void showDiaLog() {
        alertDialogBuilder.setMessage("Chon API ") ;
        alertDialogBuilder.setNegativeButton("Retrofit", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
                sendPost();
            }
        });
        alertDialogBuilder.setPositiveButton("Voley", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                String name = edtFirstName.getText().toString();
                String job = edtLastName.getText().toString();
                dialogInterface.dismiss();
                volleyPostStringRequest(name, job);
            }
        });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();

    }

    private void volleyPostStringRequest(final String name, final String job) {

        StringRequest insertRequest = new StringRequest(Request.Method.POST,
                "https://reqres.in/api/users/2",
                new com.android.volley.Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Toast.makeText(Post.this,"Volley" + response.toString(), Toast.LENGTH_SHORT).show();
            }
        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, error.toString());
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("name", name);
                params.put("job", job);

                return params;
            }
        };
        VolleySingleton.getInstance(this).getRequestQueue().add(insertRequest);
    }

    public void sendPost() {

        String name = edtFirstName.getText().toString();
        String job = edtLastName.getText().toString();
        // RxJava
        apiConnect = ApiUtils.getApi() ;
       apiConnect.savePost(name, job).enqueue(new Callback<ResponseBody>() {
           @Override
           public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
               try {
                   Toast.makeText(Post.this, "Retrofit" + ((ResponseBody) response.body()).string(), Toast.LENGTH_SHORT).show();
               } catch (IOException e) {
                   e.printStackTrace();
               }
           }

           @Override
           public void onFailure(Call<ResponseBody> call, Throwable t) {

           }
       });
    }


}
