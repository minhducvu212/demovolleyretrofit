package com.example.minhd.demoretrofitvolley.Retrofit;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.example.minhd.demoretrofitvolley.Comon.AdapterCommon;
import com.example.minhd.demoretrofitvolley.Comon.CircleRefreshLayout;
import com.example.minhd.demoretrofitvolley.Comon.Total;
import com.example.minhd.demoretrofitvolley.Comon.User;
import com.example.minhd.demoretrofitvolley.R;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RetrofitActivity extends AppCompatActivity  {

    private CircleRefreshLayout mRefreshLayout;
    private RecyclerView rclRetro;
    private AdapterCommon adapterCommon;
    private ApiConnect apiConnect;
    private List<User> users = new ArrayList<>();
    private Total total;
    private boolean check = false;
    private int curPage = 1;
    private int totalPage ;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_retrofit);
        findViewByIds();
        apiConnect = ApiUtils.getApi();
//        loadUers(1);
//        Log.d("TAGG", users.size()+"");
        rclRetro.setLayoutManager(new LinearLayoutManager(RetrofitActivity.this));
        rclRetro.setHasFixedSize(true);
        adapterCommon = new AdapterCommon(getApplicationContext(),
                users, R.layout.item, rclRetro);
        adapterCommon.setLinearLayoutManager(new LinearLayoutManager(RetrofitActivity.this));
        loadMore();
        mRefreshLayout.setOnRefreshListener(
                new CircleRefreshLayout.OnCircleRefreshListener() {
                    @Override
                    public void refreshing() {
                        mRefreshLayout.finishRefreshing();
                        refeshItem();
                        mRefreshLayout.finishRefreshing();

                    }

                    @Override
                    public void completeRefresh() {
                        mRefreshLayout.finishRefreshing();

                    }
                });
//        loadUers(1);
        loadData();

    }

    private void refeshItem() {
        new Handler().postDelayed(() -> {
            mRefreshLayout.finishRefreshing();
        }, 3000);

    }

    private void findViewByIds() {
        rclRetro = (RecyclerView) findViewById(R.id.rcl_retro);
        mRefreshLayout = (CircleRefreshLayout) findViewById(R.id.refresh_layout);
    }


    @Override
    protected void onStart() {
        super.onStart();
        Log.d("MainActivity_", "onStart");
        progressDialog = new ProgressDialog(this);
        progressDialog.show();
        progressDialog.setMessage("Loading");
//        for ( int j = 0; j < 19; j++) {
//        }
    }

    private void loadData() {
//        for (int i = 1; i < 5; i++) {
            apiConnect.getUers(curPage).enqueue(new Callback<Total>() {
                @Override
                public void onResponse(Call<Total> call, Response<Total> response) {
                    total = (Total) response.body();
                    Log.d("TAG",  total.getData().size() + "");
                    progressDialog.cancel();
                    progressDialog.dismiss();
                    if (Integer.parseInt(total.getPage()) == curPage && curPage <= 4) {
                        users.addAll(total.getData());
                    Log.d("User", users.size()+"");
                        check = true;

                        adapterCommon.upDateUser(users);
                        rclRetro.setAdapter(adapterCommon);
                        curPage++ ;
                        loadData();
                    }
                    else if(curPage == 2) {
                        if (progressDialog.isShowing()) {
                            progressDialog.cancel();
                        }
                        Total toTal = (Total) response.body()  ;
                        Log.d("REEEEEEEEEE", response.body().toString());
                        users.addAll(toTal.getData());
                        adapterCommon.notifyDataSetChanged();
                        adapterCommon.setLoading();
                        adapterCommon.addItemMore(users);
                        adapterCommon.setMoreLoading(true);
                    }

//                    Log.d("TAGG", check +"") ;
//                    if (Integer.parseInt(total.getPage()) == 2 && check && users.size() == 3) {
//                        users.addAll(total.getData());
//                        adapterCommon = new AdapterCommon(getApplicationContext(),
//                                users, R.layout.item, RetrofitActivity.this);
//                        adapterCommon.upDateUser(users);
//                        rclRetro.setAdapter(adapterCommon);
//                        check = true;
//                    }
//
//                    if (Integer.parseInt(total.getPage()) == 3 && check&& users.size() == 6) {
//                        users.addAll(total.getData());
//                        adapterCommon = new AdapterCommon(getApplicationContext(),
//                                users, R.layout.item, RetrofitActivity.this);
//                        adapterCommon.upDateUser(users);
//                        rclRetro.setAdapter(adapterCommon);
//                        check = true;
//                    }
//
//                    if (Integer.parseInt(total.getPage()) == 4 && check && users.size() == 9) {
//                        users.addAll(total.getData());
//                        adapterCommon = new AdapterCommon(getApplicationContext(),
//                                users, R.layout.item, RetrofitActivity.this);
//                        adapterCommon.upDateUser(users);
//                        rclRetro.setAdapter(adapterCommon);
//                        check = true;
//
//                    }
//
                }

                @Override
                public void onFailure(Call<Total> call, Throwable t) {

                }
            });


//        }
    }

    private void loadMore () {
        adapterCommon.setOnLoadMoreListener(new AdapterCommon.OnLoadMoreListener() {
            @Override
            public void onLoadMore() {
                if (curPage < totalPage) {
                    Log.e("ST",curPage+"");
                    users.add(null);
                    adapterCommon.notifyDataSetChanged();
                    curPage++;
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            loadData();
                        }
                    }, 2000);
                }
            }
        });
    }

}
