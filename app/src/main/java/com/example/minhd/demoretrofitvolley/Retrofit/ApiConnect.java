package com.example.minhd.demoretrofitvolley.Retrofit;

import com.example.minhd.demoretrofitvolley.Comon.Data;
import com.example.minhd.demoretrofitvolley.Comon.Total;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by minhd on 17/07/22.
 */

public interface ApiConnect {
    @GET("api/users?")
    Call<Total> getUers(@Query("page") int page);

    @GET("api/users/{id}")
    Call<Data> getUer(@Path("id") int id);

    @POST("api/users")
    @FormUrlEncoded
    Call<ResponseBody> savePost(@Field("name") String name,
                                @Field("job") String job);
    @PUT("/api/users/2")
    @FormUrlEncoded
    Call<ResponseBody> updatePost(@Field("name") String name,
                          @Field("job") String job);

}
