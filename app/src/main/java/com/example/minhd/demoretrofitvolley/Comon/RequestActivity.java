package com.example.minhd.demoretrofitvolley.Comon;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.minhd.demoretrofitvolley.R;
import com.example.minhd.demoretrofitvolley.Retrofit.ApiConnect;
import com.example.minhd.demoretrofitvolley.Retrofit.RetrofitClient;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RequestActivity extends AppCompatActivity {

    private TextView tvId, tvFirstName, tvLastName;
    private ImageView ivAva;
    private ApiConnect apiConnect;
    private List<User> users;
    private StringBuilder builder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_request);
        findViewByIds();

        Intent intent = getIntent();
        Bundle bundle = intent.getBundleExtra("id") ;
        builder = new StringBuilder("https://reqres.in/") ;

        if (1 <= bundle.getInt("ID") && bundle.getInt("ID") <= 12) {
            loadData(bundle.getInt("ID"));
        }
        else Toast.makeText(this, "ID khong thoa man", Toast.LENGTH_SHORT).show();

    }

    private void setEvents() {


    }

    private void findViewByIds() {
        tvId = (TextView) findViewById(R.id.tv_idRe);
        tvFirstName = (TextView) findViewById(R.id.tv_firstNameRe);
        tvLastName = (TextView) findViewById(R.id.tv_lastNameRe);
        ivAva = (ImageView) findViewById(R.id.iv_avaRe);

    }

    private void loadData(int id) {
        users = new ArrayList<>();
        RetrofitClient.getClient(builder.toString()).create(ApiConnect.class).getUer(id).enqueue(
                new Callback<Data>() {
                    @Override
                    public void onResponse(Call<Data> call, Response<Data> response) {
                        Data data = (Data)response.body();

                        Log.d("TAGG",  data.getData().getId()+"") ;

                        tvId.setText(data.getData().getId()+"");
                        tvFirstName.setText(data.getData().getFirstName());
                        tvLastName.setText(data.getData().getLastName());
                        Picasso.with(RequestActivity.this).load(data.getData().getAvatar()).into(ivAva);
                    }

                    @Override
                    public void onFailure(Call<Data> call, Throwable t) {

                    }
                }
        );
    }


}
