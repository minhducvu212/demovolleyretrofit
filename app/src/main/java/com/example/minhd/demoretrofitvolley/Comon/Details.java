package com.example.minhd.demoretrofitvolley.Comon;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.minhd.demoretrofitvolley.R;
import com.example.minhd.demoretrofitvolley.Retrofit.ApiConnect;
import com.example.minhd.demoretrofitvolley.Retrofit.RetrofitClient;
import com.squareup.picasso.Picasso;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by minhd on 17/07/24.
 */

public class Details extends AppCompatActivity implements View.OnClickListener {

    private TextView tvId, tvFirstName, tvLastName ;
    private ImageView ivAva ;
    public static String firstName,lastName, avatar;
    public static int id ;
    private StringBuilder builder;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.details);
        findViewByIds();
        Intent intent = getIntent();
        Bundle bundle = intent.getBundleExtra("Info") ;
        builder = new StringBuilder("https://reqres.in/") ;
        loadData(bundle.getInt("id "));

    }

    private void setEvents() {
    }

    private void findViewByIds() {
        tvId = (TextView) findViewById(R.id.tv_idDe) ;
        tvFirstName = (TextView) findViewById(R.id.tv_firstNameDe) ;
        tvLastName = (TextView) findViewById(R.id.tv_lastNameDe) ;
        ivAva = (ImageView) findViewById(R.id.iv_avaDe) ;
        findViewById(R.id.btn_add).setOnClickListener(this);
        findViewById(R.id.btn_edit).setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_add:
                Intent intentAdd = new Intent(Details.this, Post.class);
                startActivity(intentAdd);
                break;

            case R.id.btn_edit:
                Intent intentEdit = new Intent(Details.this, Edit.class);
                startActivity(intentEdit);
                break;

            default:
        }
    }

    private void loadData(int id) {
        RetrofitClient.getClient(builder.toString()).create(ApiConnect.class).getUer(id).enqueue(
                new Callback<Data>() {
                    @Override
                    public void onResponse(Call<Data> call, Response<Data> response) {
                        Data data = (Data)response.body();

                        tvId.setText(data.getData().getId()+"");
                        tvFirstName.setText(data.getData().getFirstName());
                        tvLastName.setText(data.getData().getLastName());
                        Picasso.with(Details.this).load(data.getData().getAvatar()).into(ivAva);
                    }

                    @Override
                    public void onFailure(Call<Data> call, Throwable t) {

                    }
                }
        );
    }

}
