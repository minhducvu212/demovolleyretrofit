package com.example.minhd.demoretrofitvolley.Volley;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.example.minhd.demoretrofitvolley.Comon.Edit;
import com.example.minhd.demoretrofitvolley.Comon.User;
import com.example.minhd.demoretrofitvolley.R;
import com.example.minhd.demoretrofitvolley.Comon.Post;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by minhd on 17/07/24.
 */

public class DetailsActivity extends AppCompatActivity implements View.OnClickListener {

    private TextView tvId, tvFirstName, tvLastName;
    private ImageView ivAva;
    public static String firstName, lastName, avatar;
    public static int id;
    private StringBuilder builder;
    private User user;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
        findViewByIds();
        requestSingle();

    }

    private void requestSingle() {
        Intent intent = getIntent();
        Bundle bundle = intent.getBundleExtra("Info");
        builder = new StringBuilder("https://reqres.in/");
        StringBuilder builder = new StringBuilder("https://reqres.in/api/users/");
        builder.append(bundle.getInt("id"));
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET,
                builder.toString(), null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    JSONObject jsonArray = response.getJSONObject("data");
                    Log.d("TAGGGGGG", jsonArray.toString() + "");
                    int id = jsonArray.getInt("id");
                    String firstName = jsonArray.getString("first_name");
                    String lastName = jsonArray.getString("last_name");
                    String avatar = jsonArray.getString("avatar");
                    Log.d("TAGGGGGG", id + "\t" + firstName + "\t" + lastName + "\t" + avatar +"\t");
                    tvId.setText(id + "");
                    tvFirstName.setText(firstName);
                    tvLastName.setText(lastName);
                    Picasso.with(DetailsActivity.this).load(avatar).into(ivAva);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        VolleySingleton.getInstance(this).getRequestQueue().add(jsonObjectRequest);
    }

    private void setEvents() {
    }

    private void findViewByIds() {
        tvId = (TextView) findViewById(R.id.tv_idVo);
        tvFirstName = (TextView) findViewById(R.id.tv_firstNameVo);
        tvLastName = (TextView) findViewById(R.id.tv_lastNameVo);
        ivAva = (ImageView) findViewById(R.id.iv_avaVo);
        findViewById(R.id.btn_addVo).setOnClickListener(this);
        findViewById(R.id.btn_editVo).setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_addVo:
                Intent intentAdd = new Intent(DetailsActivity.this, Post.class);
                startActivity(intentAdd);
                break;

            case R.id.btn_editVo:
                Intent intentEdit = new Intent(DetailsActivity.this, Edit.class);
                startActivity(intentEdit);
                break;

            default:
        }
    }


}
