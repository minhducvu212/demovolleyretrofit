package com.example.minhd.demoretrofitvolley.Volley;

import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.example.minhd.demoretrofitvolley.Comon.CircleRefreshLayout;
import com.example.minhd.demoretrofitvolley.Comon.User;
import com.example.minhd.demoretrofitvolley.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class VolleyActivity extends AppCompatActivity implements
        AdapterVolley.OnLoadMoreListener {

    private static final String JSON_URL = "https://reqres.in/api/users?page=";
    private static final String TAG = "MainActivity";
    private CircleRefreshLayout mRefreshLayout;
    private RecyclerView rclVolley;
    private List<User> users;
    private AdapterVolley adapterCommon;
    private List<User> list = new ArrayList<>();
    int x = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_volley);
        findViewByIds();
        users = new ArrayList<>();
        rclVolley.setLayoutManager(new LinearLayoutManager(VolleyActivity.this));
        rclVolley.setHasFixedSize(true);
        for (int i = 1; i < 17; i++) {
            for (int j =1; j < 5; j++) {
                volleyJsonArrayRequest(j);
            }

        }
        mRefreshLayout.setOnRefreshListener(
                new CircleRefreshLayout.OnCircleRefreshListener() {
                    @Override
                    public void refreshing() {
                        mRefreshLayout.finishRefreshing();
                        refeshItem();
                        mRefreshLayout.finishRefreshing();

                    }

                    @Override
                    public void completeRefresh() {
                        mRefreshLayout.finishRefreshing();

                    }
                });

    }

    private void refeshItem() {
        new Handler().postDelayed(() -> {
            mRefreshLayout.finishRefreshing();
        }, 3000);

    }

    private void findViewByIds() {
        rclVolley = (RecyclerView) findViewById(R.id.rcl_volley);
        mRefreshLayout = (CircleRefreshLayout) findViewById(R.id.refresh_layout_volley);
    }


    private void volleyJsonArrayRequest(int id) {
        StringBuilder builder = new StringBuilder("https://reqres.in/api/users?page=");
        builder.append(id);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET,
                builder.toString(), null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    JSONArray jsonArray = response.getJSONArray("data");
                    int page = response.getInt("page") ;
                    Log.d("PAGE", page +"");
                    if (page == 1 && list.size() == 0) {
                        Log.d("ID", response.toString());
                        for (int i = 0; i < jsonArray.length(); i++) {
                            int id = jsonArray.getJSONObject(i).getInt("id");
                            String firstName = jsonArray.getJSONObject(i).getString("first_name");
                            String lastName = jsonArray.getJSONObject(i).getString("last_name");
                            String avatar = jsonArray.getJSONObject(i).getString("avatar");

                            list.add(new User(id, firstName, lastName, avatar));
                            Log.d("TAGG", id + " " + list.size());
                            adapterCommon = new AdapterVolley(getApplicationContext(), list,
                                    R.layout.item, VolleyActivity.this);
                            adapterCommon.upDateUser(list);
                            rclVolley.setAdapter(adapterCommon);

                        }
                    }
                    if (page == 2 && list.size() == 3) {
                        Log.d("ID", response.toString());
                        for (int i = 0; i < jsonArray.length(); i++) {
                            int id = jsonArray.getJSONObject(i).getInt("id");
                            String firstName = jsonArray.getJSONObject(i).getString("first_name");
                            String lastName = jsonArray.getJSONObject(i).getString("last_name");
                            String avatar = jsonArray.getJSONObject(i).getString("avatar");

                            list.add(new User(id, firstName, lastName, avatar));
                            Log.d("TAGG", id + " " + list.size());
                            adapterCommon = new AdapterVolley(getApplicationContext(), list,
                                    R.layout.item, VolleyActivity.this);
                            adapterCommon.upDateUser(list);
                            rclVolley.setAdapter(adapterCommon);

                        }
                    }
                    if (page == 3 && list.size() == 6) {
                        Log.d("ID", response.toString());
                        for (int i = 0; i < jsonArray.length(); i++) {
                            int id = jsonArray.getJSONObject(i).getInt("id");
                            String firstName = jsonArray.getJSONObject(i).getString("first_name");
                            String lastName = jsonArray.getJSONObject(i).getString("last_name");
                            String avatar = jsonArray.getJSONObject(i).getString("avatar");

                            list.add(new User(id, firstName, lastName, avatar));
                            Log.d("TAGG", id + " " + list.size());
                            adapterCommon = new AdapterVolley(getApplicationContext(), list,
                                    R.layout.item, VolleyActivity.this);
                            adapterCommon.upDateUser(list);
                            rclVolley.setAdapter(adapterCommon);

                        }
                    }
                    if (page == 4 && list.size() == 9) {
                        Log.d("ID", response.toString());
                        for (int i = 0; i < jsonArray.length(); i++) {
                            int id = jsonArray.getJSONObject(i).getInt("id");
                            String firstName = jsonArray.getJSONObject(i).getString("first_name");
                            String lastName = jsonArray.getJSONObject(i).getString("last_name");
                            String avatar = jsonArray.getJSONObject(i).getString("avatar");

                            list.add(new User(id, firstName, lastName, avatar));
                            Log.d("TAGG", id + " " + list.size());
                            adapterCommon = new AdapterVolley(getApplicationContext(), list,
                                    R.layout.item, VolleyActivity.this);
                            adapterCommon.upDateUser(list);
                            rclVolley.setAdapter(adapterCommon);

                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        VolleySingleton.getInstance(this).getRequestQueue().add(jsonObjectRequest);
    }

    @Override
    public void onLoadMore() {

    }
}
