package com.example.minhd.demoretrofitvolley.Retrofit;

/**
 * Created by minhd on 17/07/22.
 */

public class ApiUtils {
    public static ApiConnect getApi() {
        return RetrofitClient.getClient("https://reqres.in/").create(ApiConnect.class);
    }
    public static ApiConnect getPostApi() {
        return RetrofitClient.getClient("https://reqres.in/api/users/").create(ApiConnect.class);

    }
}
