package com.example.minhd.demoretrofitvolley.Comon;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.minhd.demoretrofitvolley.R;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by minhd on 17/07/22.
 */

public class AdapterCommon extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context mContext;
    private List userList;
    private int rowLayout;
    private final int VIEW_ITEM = 1;
    private final int VIEW_PROG = 0;
    private RecyclerView recyclerView ;
//    private ArrayList<User> itemList;

    private OnLoadMoreListener onLoadMoreListener;
    private LinearLayoutManager mLinearLayoutManager;

    private boolean isMoreLoading = false;
    private int visibleThreshold = 1;
    int firstVisibleItem, visibleItemCount, totalItemCount;


    public interface OnLoadMoreListener {
        void onLoadMore();
    }

    public AdapterCommon(Context context, List<User> users, int row, RecyclerView rcl) {
        mContext = context;
        userList = users;
        rowLayout = row;
        recyclerView = rcl ;
        loadMore();
    }

    public void setLinearLayoutManager(LinearLayoutManager linearLayoutManager) {
        this.mLinearLayoutManager = linearLayoutManager;
    }

    public void loadMore() {
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
                int totalItemCount = linearLayoutManager.getItemCount();
                int lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
                if (lastVisibleItem == (totalItemCount -1) && !isMoreLoading){
                    if (onLoadMoreListener != null){
                        onLoadMoreListener.onLoadMore();
                    }
                    isMoreLoading = true;
                }
            }
        });
    }

    public void setLoading(){
        isMoreLoading = false;
    }

    public void setOnLoadMoreListener(OnLoadMoreListener onLoadMoreListener) {
        this.onLoadMoreListener = onLoadMoreListener;
    }


    @Override
    public int getItemViewType(int position) {
        return userList.get(position) != null ? VIEW_ITEM : VIEW_PROG;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Log.d("ViewType", viewType+"");

        if (viewType == VIEW_ITEM) {
            return new MyHolder(LayoutInflater.from(parent.getContext()).
                    inflate(R.layout.item, parent, false));
        } else {
            return new ProgressViewHolder(LayoutInflater.from(parent.getContext()).
                    inflate(R.layout.progress_layout, parent, false));
        }

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        User user = (User) userList.get(position);
        TextView tvId = holder.itemView.findViewById(R.id.tv_id);
        tvId.setText(user.getId() + "");
        TextView tvFirstName = holder.itemView.findViewById(R.id.tv_firstName);
        tvFirstName.setText(user.getFirstName());
        TextView tvLastName = holder.itemView.findViewById(R.id.tv_lastName);
        tvLastName.setText(user.getLastName());
        ImageView ivAva = holder.itemView.findViewById(R.id.iv_ava);
        Picasso.with(mContext).load(user.getAvatar()).into(ivAva);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mContext, Details.class);
                Bundle bundle = new Bundle();
                bundle.putInt("id", user.getId());
                bundle.putString("firstName", user.getFirstName());
                bundle.putString("lastName", user.getLastName());
                bundle.putString("avatar", user.getAvatar());
                intent.putExtra("Info", bundle) ;
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                mContext.startActivity(intent);
            }
        });

    }

    public void addAll(List<User> lst) {
        lst.addAll(lst);
        notifyDataSetChanged();
    }

    public void addItemMore(List<User> lst) {
        lst.addAll(lst);
        notifyItemRangeChanged(0, lst.size());
    }

    public void setMoreLoading(boolean isMoreLoading) {
        this.isMoreLoading = isMoreLoading;
    }

    public void setProgressMore(final boolean isProgress) {
        if (isProgress) {
            new Handler().post(new Runnable() {
                @Override
                public void run() {
                    userList.add(null);
                    notifyItemInserted(userList.size() - 1);
                }
            });
        } else {
            userList.remove(userList.size() - 1);
            notifyItemRemoved(userList.size());
        }
    }

    static class ProgressViewHolder extends RecyclerView.ViewHolder {
        public ProgressBar pBar;

        public ProgressViewHolder(View v) {
            super(v);
            pBar = (ProgressBar) v.findViewById(R.id.more_progress);
        }
    }


    @Override
    public int getItemCount() {
        return userList.size();
    }

    public void upDateUser(List<User> data) {
        userList = data;
//        notifyDataSetChanged();
    }

    static class MyHolder extends RecyclerView.ViewHolder {

        public TextView tvId, tvFirstName, tvLastName;
        public ImageView ivAva;


        public MyHolder(View itemView) {
            super(itemView);

            tvId = itemView.findViewById(R.id.tv_id);
            tvFirstName = itemView.findViewById(R.id.tv_firstName);
            tvLastName = itemView.findViewById(R.id.tv_lastName);
            ivAva = itemView.findViewById(R.id.iv_ava);

        }
    }

}
